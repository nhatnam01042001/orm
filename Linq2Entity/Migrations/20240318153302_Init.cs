﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace Linq2Entity.Migrations
{
    /// <inheritdoc />
    public partial class Init : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Products",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Price = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    Quantity = table.Column<int>(type: "int", nullable: false),
                    ManufactureDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    Category = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Origin = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Products", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "Products",
                columns: new[] { "Id", "Category", "IsActive", "ManufactureDate", "Name", "Origin", "Price", "Quantity" },
                values: new object[,]
                {
                    { 1, "Electronics", true, new DateTime(2022, 3, 10, 0, 0, 0, 0, DateTimeKind.Unspecified), "Laptop", "USA", 1200m, 15 },
                    { 2, "Electronics", true, new DateTime(2022, 3, 12, 0, 0, 0, 0, DateTimeKind.Unspecified), "Smartphone", "China", 800m, 20 },
                    { 3, "Electronics", false, new DateTime(2022, 3, 14, 0, 0, 0, 0, DateTimeKind.Unspecified), "Tablet", "Korea", 500m, 10 },
                    { 4, "Electronics", true, new DateTime(2022, 3, 16, 0, 0, 0, 0, DateTimeKind.Unspecified), "Headphones", "Japan", 100m, 30 },
                    { 5, "Electronics", false, new DateTime(2022, 3, 18, 0, 0, 0, 0, DateTimeKind.Unspecified), "Keyboard", "Taiwan", 50m, 25 },
                    { 6, "Electronics", true, new DateTime(2022, 3, 20, 0, 0, 0, 0, DateTimeKind.Unspecified), "Mouse", "China", 30m, 40 },
                    { 7, "Electronics", true, new DateTime(2022, 3, 22, 0, 0, 0, 0, DateTimeKind.Unspecified), "Monitor", "Korea", 300m, 5 },
                    { 8, "Electronics", false, new DateTime(2022, 3, 24, 0, 0, 0, 0, DateTimeKind.Unspecified), "Printer", "Japan", 200m, 8 },
                    { 9, "Electronics", true, new DateTime(2022, 3, 26, 0, 0, 0, 0, DateTimeKind.Unspecified), "Camera", "China", 400m, 12 },
                    { 10, "Electronics", true, new DateTime(2022, 3, 28, 0, 0, 0, 0, DateTimeKind.Unspecified), "Speaker", "USA", 150m, 18 },
                    { 11, "Accessories", true, new DateTime(2022, 3, 30, 0, 0, 0, 0, DateTimeKind.Unspecified), "External Hard Drive", "Taiwan", 80m, 22 },
                    { 12, "Electronics", false, new DateTime(2022, 4, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Router", "China", 120m, 17 },
                    { 13, "Wearables", true, new DateTime(2022, 4, 3, 0, 0, 0, 0, DateTimeKind.Unspecified), "Smartwatch", "Korea", 200m, 13 },
                    { 14, "Accessories", true, new DateTime(2022, 4, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), "Printer Ink", "USA", 40m, 28 },
                    { 15, "Furniture", false, new DateTime(2022, 4, 7, 0, 0, 0, 0, DateTimeKind.Unspecified), "Gaming Chair", "China", 250m, 9 },
                    { 16, "Accessories", true, new DateTime(2022, 4, 9, 0, 0, 0, 0, DateTimeKind.Unspecified), "USB Flash Drive", "Taiwan", 20m, 35 },
                    { 17, "Accessories", true, new DateTime(2022, 4, 11, 0, 0, 0, 0, DateTimeKind.Unspecified), "Wireless Earbuds", "USA", 100m, 24 },
                    { 18, "Electronics", false, new DateTime(2022, 4, 13, 0, 0, 0, 0, DateTimeKind.Unspecified), "External Monitor", "Japan", 400m, 7 },
                    { 19, "Home", true, new DateTime(2022, 4, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), "Desk Lamp", "China", 30m, 19 },
                    { 20, "Accessories", true, new DateTime(2022, 4, 17, 0, 0, 0, 0, DateTimeKind.Unspecified), "Power Bank", "Taiwan", 50m, 14 }
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Products");
        }
    }
}
