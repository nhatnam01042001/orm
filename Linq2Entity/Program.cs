﻿Console.WriteLine("hello");

// khởi tạo đối tượng context đại diện cho 1 database
AppDbContext dbContext = new AppDbContext();


// Lấy ra tất cả sản phẩm có giá lớn hơn 200:
List<Product> expensiveProducts = dbContext.Products.Where(p => p.Price > 200).ToList();

foreach (Product product in expensiveProducts)
{
    Console.WriteLine($"{product.Id} {product.Name} {product.Price}");
}