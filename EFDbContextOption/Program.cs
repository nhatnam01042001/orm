﻿using Microsoft.EntityFrameworkCore;

DbContextOptionsBuilder<AppDbContext> optionsBuilder = new DbContextOptionsBuilder<AppDbContext>();
optionsBuilder.UseInMemoryDatabase("testDatabase");

AppDbContext context = new AppDbContext(optionsBuilder.Options);

// tạo danh sách person sẽ thêm vào database
List<Person> personData = new List<Person>
{
    new Person
    {
        Name = "A",
    },
    new Person
    {
        Name = "B",
    }
};

// đánh dấu là thêm vào database
context.Persons.AddRange(personData);

// lưu mọi sự thay đổi
context.SaveChanges();

List<Person> persons = await context.Persons.ToListAsync();

foreach (var person in persons)
{
    Console.WriteLine($"{person.Id} {person.Name}");
}

public class AppDbContext : DbContext
{
    public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
    {
    }

    public DbSet<Person> Persons { get; set; }
}

public class Person
{
    public int Id { get; set; }
    public string Name { get; set; }
}
